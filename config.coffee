exports.config =
  paths:
    'public': 'public'
  assets: [
    'images'
    'assets'
    'fonts'
  ]
  vendor: [
    'fonts'
  ]
  coffeelint:
    pattern: /^app\/.*\.coffee$/
    options:
      indentation:
        value: 2
        level: "error"
      max_line_length:
        value: 80
        level: "ignore"
  files:
    javascripts:
      joinTo:
        'javascripts/app.js': /^app/
        'javascripts/vendor.js':  /^(bower_components|vendor)/
        'test/js/test.js': /^test(\/|\\)(?!vendor)/
        'test/js/test-vendor.js': /^test(\/|\\)(?=vendor)/
        order:
          # Files in `vendor` directories are compiled before other files
          # even if they aren't specified in order.
          before: [

          ]
          after: [
             'vendor/scripts/swag/swag.js'
          ]
    stylesheets:
      defaultExtension: 'styl'
      joinTo: 'css/app.css'
      order:
        before: [
          'app/css/*'
        ]
        after: [

        ]
    templates:
      defaultExtension: 'hbs'
      joinTo: 'javascripts/app.js'
  # Settings of web server that will run with `brunch watch [--server]`.
  server:
    port: 3335
    path: 'server.coffee'
    publicPath: 'public'
    run: yes  #Run even without `--server` option?
  plugins:
    coffeelint:
      pattern: /^app\/.*\.coffee$/
      options: require('./coffeelint.json')

