(function(/*! Brunch !*/) {
  'use strict';

  var globals = typeof window !== 'undefined' ? window : global;
  if (typeof globals.require === 'function') return;

  var modules = {};
  var cache = {};

  var has = function(object, name) {
    return ({}).hasOwnProperty.call(object, name);
  };

  var expand = function(root, name) {
    var results = [], parts, part;
    if (/^\.\.?(\/|$)/.test(name)) {
      parts = [root, name].join('/').split('/');
    } else {
      parts = name.split('/');
    }
    for (var i = 0, length = parts.length; i < length; i++) {
      part = parts[i];
      if (part === '..') {
        results.pop();
      } else if (part !== '.' && part !== '') {
        results.push(part);
      }
    }
    return results.join('/');
  };

  var dirname = function(path) {
    return path.split('/').slice(0, -1).join('/');
  };

  var localRequire = function(path) {
    return function(name) {
      var dir = dirname(path);
      var absolute = expand(dir, name);
      return globals.require(absolute, path);
    };
  };

  var initModule = function(name, definition) {
    var module = {id: name, exports: {}};
    cache[name] = module;
    definition(module.exports, localRequire(name), module);
    return module.exports;
  };

  var require = function(name, loaderPath) {
    var path = expand(name, '.');
    if (loaderPath == null) loaderPath = '/';

    if (has(cache, path)) return cache[path].exports;
    if (has(modules, path)) return initModule(path, modules[path]);

    var dirIndex = expand(path, './index');
    if (has(cache, dirIndex)) return cache[dirIndex].exports;
    if (has(modules, dirIndex)) return initModule(dirIndex, modules[dirIndex]);

    throw new Error('Cannot find module "' + name + '" from '+ '"' + loaderPath + '"');
  };

  var define = function(bundle, fn) {
    if (typeof bundle === 'object') {
      for (var key in bundle) {
        if (has(bundle, key)) {
          modules[key] = bundle[key];
        }
      }
    } else {
      modules[bundle] = fn;
    }
  };

  var list = function() {
    var result = [];
    for (var item in modules) {
      if (has(modules, item)) {
        result.push(item);
      }
    }
    return result;
  };

  globals.require = require;
  globals.require.define = define;
  globals.require.register = define;
  globals.require.list = list;
  globals.require.brunch = true;
})();
require.register("helpers/handlebars", function(exports, require, module) {
module.exports = Handlebars.registerHelper("pluralize", function(num, singular, plural) {
  if (parseInt(num, 10) === 1) {
    return singular;
  } else {
    if (typeof plural === "string") {
      return plural;
    } else {
      return singular + "s";
    }
  }
});

Handlebars.registerHelper("pluralCount", function(num, singular, plural) {
  return number + " " + Handlebars.helpers.pluralize.apply(this, arguments);
});

Handlebars.registerHelper("dateFormat", function(context, block) {
  var f;
  if (window.moment) {
    f = block.hash.format || "MMM DD, YYYY hh:mm:ss A";
    return moment(context).zone((new Date()).getTimezoneOffset()).format(f);
  } else {
    return context;
  }
});
});

;require.register("helpers/helpers", function(exports, require, module) {
(function (root, factory) {
    if (typeof exports === 'object') {
        module.exports = factory(require('handlebars'));
    } else if (typeof define === 'function' && define.amd) {
        define(['handlebars'], factory);
    } else {
        root.HandlebarsHelpersRegistry = factory(root.Handlebars);
    }
}(this, function (Handlebars) {

    var isArray = function(value) {
        return Object.prototype.toString.call(value) === '[object Array]';
    }

    var ExpressionRegistry = function() {
        this.expressions = [];
    };

    ExpressionRegistry.prototype.add = function (operator, method) {
        this.expressions[operator] = method;
    };

    ExpressionRegistry.prototype.call = function (operator, left, right) {
        if ( ! this.expressions.hasOwnProperty(operator)) {
            throw new Error('Unknown operator "'+operator+'"');
        }

        return this.expressions[operator](left, right);
    };

    var eR = new ExpressionRegistry;
    eR.add('not', function(left, right) {
        return left != right;
    });
    eR.add('>', function(left, right) {
        return left > right;
    });
    eR.add('<', function(left, right) {
        return left < right;
    });
    eR.add('>=', function(left, right) {
        return left >= right;
    });
    eR.add('<=', function(left, right) {
        return left <= right;
    });
    eR.add('===', function(left, right) {
        return left === right;
    });
    eR.add('!==', function(left, right) {
        return left !== right;
    });
    eR.add('in', function(left, right) {
        if ( ! isArray(right)) {
            right = right.split(',');
        }
        return right.indexOf(left) !== -1;
    });

    var isHelper = function() {
        var args = arguments
        ,   left = args[0]
        ,   operator = args[1]
        ,   right = args[2]
        ,   options = args[3]
        ;

        if (args.length == 2) {
            options = args[1];
            if (left) return options.fn(this);
            return options.inverse(this);
        }

        if (args.length == 3) {
            right = args[1];
            options = args[2];
            if (left == right) return options.fn(this);
            return options.inverse(this);
        }

        if (eR.call(operator, left, right)) {
            return options.fn(this);
        }
        return options.inverse(this);
    };

    Handlebars.registerHelper('is', isHelper);

    Handlebars.registerHelper('nl2br', function(text) {
        var nl2br = (text + '').replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1' + '<br>' + '$2');
        return new Handlebars.SafeString(nl2br);
    });

    Handlebars.registerHelper('log', function() {
        console.log(['Values:'].concat(
            Array.prototype.slice.call(arguments, 0, -1)
        ));
    });

    Handlebars.registerHelper('debug', function() {
        console.log('Context:', this);
        console.log(['Values:'].concat(
            Array.prototype.slice.call(arguments, 0, -1)
        ));
    });

    return eR;

}));
});

require.register("initialize", function(exports, require, module) {
if (this.Application == null) {
  this.Application = {
    host: "http://" + window.location.host
  };
}

this.View = require('./lib/view');

this.Router = require('./lib/router');

this.Model = require('./lib/model');

this.Collection = require('./lib/collection');

this.LocalStorageCollection = require('./lib/local_storage_collection');

this.MainView = require('./views/main_view');

this.PathUtils = require('./utils/path_utils');

this.Logger = require('./utils/logger_util');

this.DateUtils = require('./utils/date_utils');

this.ScriptUtils = require('./utils/script_utils');

this.TrackingUtil = require('./utils/tracking_util');

this.isTouchDevice = /android|webos|iphone|ipod|ipad|blackberry|iemobile/i.test(navigator.userAgent.toLowerCase());

$(document).ready(function() {
  var mainView;
  _.extend(Application, Backbone.Events);
  mainView = new MainView();
  $('body').append(mainView.$el);
});
});

;require.register("lib/collection", function(exports, require, module) {
var Collection,
  __hasProp = {}.hasOwnProperty,
  __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

Collection = (function(_super) {
  __extends(Collection, _super);

  function Collection(items, defaults) {
    this.searchFields = (defaults != null ? defaults.searchFields : void 0) || ['summary', 'name'];
    Collection.__super__.constructor.call(this, items, defaults);
  }

  Collection.prototype.search = function(query, callback) {
    var context, matches, pattern, searchFields;
    searchFields = this.searchFields;
    pattern = new RegExp($.trim(query).replace(RegExp(" ", "g"), "|"), "i");
    context = this.filteredAppCollection || this;
    matches = context.filter(function(model) {
      var doesMatch, field, k, v, _i, _len, _ref;
      doesMatch = false;
      for (_i = 0, _len = searchFields.length; _i < _len; _i++) {
        field = searchFields[_i];
        if (!field) {
          return false;
        }
        _ref = model.attributes;
        for (k in _ref) {
          v = _ref[k];
          if (k === field && v.match(pattern)) {
            doesMatch = true;
          }
        }
        return doesMatch;
      }
    });
    callback.call(this, matches);
  };

  return Collection;

})(Backbone.Collection);

module.exports = Collection;
});

;require.register("lib/local_storage_collection", function(exports, require, module) {
var LocalStorageCollection,
  __hasProp = {}.hasOwnProperty,
  __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

LocalStorageCollection = (function(_super) {
  __extends(LocalStorageCollection, _super);

  LocalStorageCollection.prototype.cacheTTL = 60;

  function LocalStorageCollection(objects, defaults) {
    var localStorageName, ts;
    this.name = localStorageName = (defaults != null ? defaults.localStorageName : void 0) || this.localStorageName;
    ts = localStorage.getItem("" + localStorageName + "-timestamp");
    if (ts) {
      Logger.debug(localStorageName, ' Timestamp:', ts, 'cacheTTL', this.cacheTTL, 'Seconds Living:', (new Date() - new Date(ts)) / 1000, 'Replace?', (new Date() - new Date(ts)) / 1000 > this.cacheTTL);
    } else {
      Logger.debug(localStorageName, ' Never loaded, querying API server');
    }
    if (ts && (new Date() - new Date(ts)) / 1000 < this.cacheTTL && (typeof navigator !== "undefined" && navigator !== null ? navigator.onLine : void 0)) {
      this.sync = Backbone.LocalStorage.sync;
    } else {
      this.sync = Backbone.sync;
      this.listenTo(this, 'sync', this.updateTimestamp);
    }
    this.localStorageCollection = new Store(localStorageName);
    LocalStorageCollection.__super__.constructor.call(this, objects, defaults);
  }

  LocalStorageCollection.prototype.storeMany = function(models) {
    var m, _i, _len, _results;
    _results = [];
    for (_i = 0, _len = models.length; _i < _len; _i++) {
      m = models[_i];
      _results.push(this.store(m));
    }
    return _results;
  };

  LocalStorageCollection.prototype.store = function(model) {
    return this.localStorageCollection.create(model);
  };

  LocalStorageCollection.prototype.destroy = function(model) {
    return this.localStorageCollection.destroy(model);
  };

  LocalStorageCollection.prototype.findAll = function() {
    var model, obj, _i, _len, _ref, _ref1, _results;
    model = this.model || Model;
    _ref1 = (_ref = this.localStorageCollection) != null ? _ref.findAll() : void 0;
    _results = [];
    for (_i = 0, _len = _ref1.length; _i < _len; _i++) {
      obj = _ref1[_i];
      _results.push(new model(obj));
    }
    return _results;
  };

  LocalStorageCollection.prototype.indexOfLocalStorage = function(model) {
    var a, m, result;
    a = this.findAll();
    result = (function() {
      var _i, _len, _results;
      _results = [];
      for (_i = 0, _len = a.length; _i < _len; _i++) {
        m = a[_i];
        if (model.get('guid') === m.get('guid')) {
          _results.push(item);
        }
      }
      return _results;
    })();
    return a.indexOf(result);
  };

  LocalStorageCollection.prototype.updateTimestamp = function() {
    return localStorage.setItem("" + this.name + "-timestamp", new Date().toISOString());
  };

  LocalStorageCollection.prototype.save = function() {
    var m, _i, _len, _ref, _results;
    _ref = this.models;
    _results = [];
    for (_i = 0, _len = _ref.length; _i < _len; _i++) {
      m = _ref[_i];
      _results.push(this.store(m));
    }
    return _results;
  };

  return LocalStorageCollection;

})(Collection);

module.exports = LocalStorageCollection;
});

;require.register("lib/model", function(exports, require, module) {
var Model,
  __hasProp = {}.hasOwnProperty,
  __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

Model = (function(_super) {
  __extends(Model, _super);

  function Model(attrs, options) {
    Model.__super__.constructor.call(this, attrs, options);
  }

  Model.prototype.initialize = function(attrs, options) {
    if (attrs != null ? attrs.url : void 0) {
      this.url = attrs != null ? attrs.url : void 0;
    }
    if (!this.get('guid')) {
      this.set({
        guid: this.generateGuid()
      });
    }
    return Model.__super__.initialize.call(this, attrs, options);
  };

  Model.generateGuid = function() {
    var S4, guid;
    S4 = function() {
      return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
    };
    guid = function() {
      return S4() + S4() + "-" + S4() + "-" + S4() + "-" + S4() + "-" + S4() + S4() + S4();
    };
    return guid();
  };

  Model.prototype.generateGuid = function() {
    var S4, guid;
    S4 = function() {
      return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
    };
    guid = function() {
      return S4() + S4() + "-" + S4() + "-" + S4() + "-" + S4() + "-" + S4() + S4() + S4();
    };
    return guid();
  };

  return Model;

})(Backbone.Model);

module.exports = Model;
});

;require.register("lib/paging_collection", function(exports, require, module) {
var PagingCollection, _ref,
  __hasProp = {}.hasOwnProperty,
  __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
  __indexOf = [].indexOf || function(item) { for (var i = 0, l = this.length; i < l; i++) { if (i in this && this[i] === item) return i; } return -1; };

PagingCollection = (function(_super) {
  __extends(PagingCollection, _super);

  function PagingCollection() {
    _ref = PagingCollection.__super__.constructor.apply(this, arguments);
    return _ref;
  }

  PagingCollection.prototype.ignore = ['length', 'id', 'appId', '_listenId', 'count'];

  PagingCollection.prototype.defaults = {
    page_size: 10,
    page: 1
  };

  PagingCollection.prototype.idAttribute = 'appId';

  PagingCollection.prototype.url = function() {
    var k, params, v;
    params = _({}).extend(this.defaults);
    for (k in this) {
      if (!__hasProp.call(this, k)) continue;
      v = this[k];
      if (_(v).isString() || _(v).isNumber()) {
        if (__indexOf.call(this.ignore, k) < 0) {
          params[k] = v;
        }
      }
    }
    return "#?" + ($.param(params));
  };

  PagingCollection.prototype.initialize = function(models, options) {
    if (options == null) {
      options = {};
    }
    this.appId = options != null ? options.appId : void 0;
    return _(this).extend(options);
  };

  PagingCollection.prototype.parse = function(response) {
    this.page = (response != null ? response.page : void 0) || 1;
    this.count = response != null ? response.count : void 0;
    return (response != null ? response.results : void 0) || [];
  };

  PagingCollection.prototype.nextPage = function() {
    return this.goToPage((this.page || 0) + 1);
  };

  PagingCollection.prototype.previousPage = function() {
    return this.goToPage((this.page || 1) - 1);
  };

  PagingCollection.prototype.goToPage = function(page) {
    this.page = page != null ? page : 0;
    return this.fetch();
  };

  return PagingCollection;

})(Collection);
});

;require.register("lib/router", function(exports, require, module) {
var Router, _ref,
  __hasProp = {}.hasOwnProperty,
  __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

Router = (function(_super) {
  __extends(Router, _super);

  function Router() {
    _ref = Router.__super__.constructor.apply(this, arguments);
    return _ref;
  }

  Router.prototype.routes = {};

  Router.prototype.initialize = function(opts) {
    var _this = this;
    if (opts == null) {
      opts = {};
    }
    return $(document).on('click', 'a[href]:not([target]):not([data-bypass])', function(e) {
      var href, matched;
      href = $(e.currentTarget).attr('href');
      if (/^(https?|mailto|javascript|#)/.test(href)) {
        return;
      }
      matched = _(Backbone.history.handlers).any(function(h) {
        return h.route.test(href.slice(1));
      });
      if (matched) {
        e.preventDefault();
        return _this.navigate(href, {
          trigger: true
        });
      }
    });
  };

  Router.prototype.getQueryParamObject = function() {
    var hash, query, vars;
    query = window.location.search.substring(1);
    hash = {};
    if (_.isEmpty(query)) {
      return hash;
    }
    vars = query.split("&");
    _.each(vars, function(el, index) {
      var pair;
      pair = el.split("=");
      return hash[decodeURIComponent(pair[0])] = decodeURIComponent(pair[1]);
    });
    return hash;
  };

  Router.prototype.removeUrlParam = function(key) {
    var i, pairs, prefix, search, url, urlparts;
    search = '';
    url = window.location.search.split('?');
    urlparts = url;
    if (urlparts.length >= 2) {
      prefix = encodeURIComponent(key) + "=";
      pairs = urlparts[1].split(/[&;]/g);
      i = pairs.length;
      while (i-- > 0) {
        if (pairs[i].lastIndexOf(prefix, 0) !== -1) {
          pairs.splice(i, 1);
        }
        search = urlparts[0] + "?" + pairs.join("&");
      }
    } else {
      search;
    }
    search = search === "?" ? "" : search;
    return window.history.pushState({}, document.title, "" + window.location.pathname + search);
  };

  Router.prototype.updatePath = function(path) {
    return window.history.pushState({}, document.title, "" + path);
  };

  Router.prototype.getQueryPath = function(hash, extend) {
    var url;
    if (_.isEmpty(hash)) {
      return;
    }
    extend = extend || {};
    hash = _.extend(this.getQueryParamObject(), hash, extend);
    url = "";
    _.each(hash, function(value, key) {
      var end, pad;
      pad = url === "" ? "?" : "";
      end = url === "" ? "" : "&";
      return url = "" + url + pad + key + "=" + value + "&";
    });
    return url.slice(0, url.length - 1);
  };

  Router.prototype.getUpdatedUrlPath = function(hash, extend) {
    var url;
    if (_.isEmpty(hash)) {
      return;
    }
    url = this.getQueryPath(hash, extend);
    return "" + window.location.pathname + url;
  };

  Router.prototype.updateUrlParams = function(hash, extend, replaceState) {
    if (replaceState) {
      return window.history.replaceState({}, document.title, this.getUpdatedUrlPath(hash, extend));
    } else {
      return window.history.pushState({}, document.title, this.getUpdatedUrlPath(hash, extend));
    }
  };

  Router.prototype.getParameterByName = function(name) {
    var regex, results;
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    regex = new RegExp("[\\?&]" + name + "=([^&#]*)");
    results = regex.exec(location.search);
    if (results == null) {
      return "";
    } else {
      return decodeURIComponent(results[1].replace(/\+/g, " "));
    }
  };

  Router.prototype.execute = function(callback, args) {
    if (callback) {
      callback.apply(this, args);
    }
    if (typeof window !== "undefined" && window !== null ? window.ga : void 0) {
      ga('send', 'pageview');
    }
  };

  return Router;

})(Backbone.Router);

module.exports = Router;
});

;require.register("lib/view", function(exports, require, module) {
var Model, View,
  __hasProp = {}.hasOwnProperty,
  __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
  __slice = [].slice;

Model = require('lib/model');

View = (function(_super) {
  __extends(View, _super);

  View.prototype.name = null;

  View.prototype.autoRender = false;

  View.prototype.autoRemove = true;

  View.prototype.rendered = false;

  View.prototype.template = function() {
    return '';
  };

  /*
  @return instance of View
  */


  function View(options) {
    View.__super__.constructor.call(this, options);
  }

  /*
  Kicks off the shit
  @return null
  */


  View.prototype.initialize = function() {
    if (!(typeof Application !== "undefined" && Application !== null ? Application.viewMap : void 0)) {
      Application.viewMap = [];
    }
    if ((this.model != null) && this.autoRemove) {
      this.listenTo(this.model, 'remove', this.remove);
    }
    this.name = this.name || this.constructor.name;
    if (this.autoRender === true) {
      this.render();
    }
    this.trigger("" + this.cid + ":initialize", this);
    this.prepareModels();
    return this.prepareViews();
  };

  /*
  Mixin'd in views
  example: @include ('Coverflow')
  */


  View.prototype.events = function() {
    var events, k, mobileKey, v;
    events = {};
    for (k in this) {
      v = this[k];
      if (/[\w+]+_events$/.test(k)) {
        _(events).extend(v);
      }
    }
    for (k in events) {
      v = events[k];
      if (!(/click/.test(k) && isTouchDevice)) {
        continue;
      }
      mobileKey = k.replace('click', 'touchstart');
      events[mobileKey] = v;
      delete events[k];
    }
    return events;
  };

  /*
  Statics:
  Include a mixin, found in ./views/mixins
  example: @include ('Coverflow')
  */


  View.include = function() {
    var key, mixin, mixins, value, _i, _len, _results;
    mixins = 1 <= arguments.length ? __slice.call(arguments, 0) : [];
    _results = [];
    for (_i = 0, _len = mixins.length; _i < _len; _i++) {
      mixin = mixins[_i];
      _results.push((function() {
        var _ref, _results1;
        _ref = Application.mixins[mixin].prototype;
        _results1 = [];
        for (key in _ref) {
          if (!__hasProp.call(_ref, key)) continue;
          value = _ref[key];
          _results1.push(this.prototype[key] = value);
        }
        return _results1;
      }).call(this));
    }
    return _results;
  };

  /*
  Include a mixin, found in ./views/mixins
  example: include ('Coverflow')
  */


  View.prototype.include = function() {
    var key, mixin, mixins, value, _i, _len, _ref;
    mixins = 1 <= arguments.length ? __slice.call(arguments, 0) : [];
    for (_i = 0, _len = mixins.length; _i < _len; _i++) {
      mixin = mixins[_i];
      _ref = AgentBidder.mixins[mixin].prototype;
      for (key in _ref) {
        if (!__hasProp.call(_ref, key)) continue;
        value = _ref[key];
        this[key] = value;
      }
    }
    if (this["" + mixin + "Init"]) {
      this["" + mixin + "Init"]();
    }
    return this.delegateEvents();
  };

  /*
  Dispatch an event to on the view's element
  
  @param eventName - String - name of event to dispatch
  @params ...rest - parameters to pass to event handlers
  @return View - current view
  */


  View.prototype.dispatch = function(eventName) {
    var arguments_;
    arguments_ = arguments;
    this.$el.trigger(eventName, _.rest(arguments_));
    return this;
  };

  /*
  delegate events, called automaticall after render
  @param events  - object hash of events to listen for
  */


  View.prototype.delegateEvents = function(events) {
    Backbone.View.prototype.delegateEvents.call(this, events);
    View.__super__.delegateEvents.call(this, _(this.events != null).isFunction() ? this.events() || this.events : void 0);
    if (this.eventNamespace) {
      return this.$el.bind("DOMElementRemoved" + this.eventNamespace, _.bind(this.remove, this));
    }
  };

  /*
  convienence method for wiping out sub views
  ex: this.contentView.empty()
  */


  View.prototype.empty = function() {
    return this.$el.empty();
  };

  /*
  Append current view to another view
  ex: this.contentView.appendToView(this.$('#content')) appends contentView to the dom element #content
  
  @param view - element/jQuery/View - an element, View or jQuery Object to add the current view to
  @return View - current view
  */


  View.prototype.appendToView = function(view) {
    var el;
    el = this.$el;
    if (view instanceof Backbone.View) {
      view.$el.append(el);
    } else {
      $(view).append(el);
    }
    this.render();
    return this;
  };

  /*
  Prepend current view to another view
  
  @param view - element/jQuery/View - an element, View or jQuery Object to prepend the current view to
  @return View - current view
  */


  View.prototype.prependToView = function(view) {
    var el;
    el = this.$el;
    if (view instanceof Backbone.View) {
      view.$el.prepend(el);
    } else {

    }
    $(view).prepend(el);
    this.render();
    return this;
  };

  /*
  Get the html buffer to render into the element
  
  @return Object - property object to render into html
  */


  View.prototype.getRenderBuffer = function(data) {
    return this.template(data);
  };

  /*
  Render the view with the provided buffer.
  
  @param buffer - String - html to render into the view.
  */


  View.prototype.renderBuffer = function(buffer) {
    return this.$el.html(buffer);
  };

  /*
  Get the model data to render the view.
  @return Object - property object that view will be rendered from
  */


  View.prototype.getRenderData = function() {
    var o, _ref;
    o = {
      host: Application.host
    };
    return _.extend({}, o, (_ref = this.model) != null ? _ref.toJSON() : void 0);
  };

  /*
  Get the model data to render the view.
  @return Object - property object that view will be rendered from
  */


  View.prototype.render = function() {
    var _ref, _ref1;
    this.trigger("" + this.cid + ":render:before", this);
    this.beforeRender();
    this.$el.attr('data-cid', this.cid);
    if (this.model) {
      this.$el.attr('data-model-cid', (_ref = this.model) != null ? _ref.cid : void 0);
      this.$el.attr('data-model-id', (_ref1 = this.model) != null ? _ref1.id : void 0);
    }
    this.html(this.template(this.getRenderData()));
    this.rendered = true;
    Application.viewMap[this.cid] = this;
    this.trigger("" + this.cid + ":render:after", this);
    this.afterRender();
    return this;
  };

  /*
  Get the model data to render the view.
  @return Object - Convenience function for doToggle
  */


  View.prototype.toggleClass = function(className, doToggle) {
    this.$el.toggleClass(className, doToggle);
    return this;
  };

  View.prototype.html = function(dom) {
    this.$el.html(dom);
    this.trigger("" + this.cid + ":" + (this.rendered ? 'update' : 'render'), this);
    return this.$el;
  };

  /*
  Append a view to the current view
  ex: this.append(this.contentView)
  
  @param view - element/jQuery/View - an element, View or jQuery Object to append to the current view's DOM element
  @return View - current view
  */


  View.prototype.append = function(view) {
    if (view instanceof View) {
      view.appendTo(this);
    } else if (view instanceof Backbone.View) {
      this.$el.append(view.$el);
      view.render();
    } else {
      this.$el.append(view);
    }
    this.trigger("" + this.cid + ":" + (this.rendered ? 'update' : 'render'), this);
    return this;
  };

  /*
  Insert a view into the currentView's element, replacing it
  
  @param view - element/jQuery/View - an element, View or jQuery Object
  @param el - element you wan to insert at in the current view
  @return View - current view
  */


  View.prototype.insertAtElement = function(view, el) {
    if (view instanceof View) {
      if (!(view != null ? view.$el : void 0)) {
        view.render();
      }
      view.appendTo(this.$(el));
    } else if (view instanceof Backbone.View) {
      this.$(el).append(view.$el);
      view.render();
    } else {
      this.$el.find(el).append(view);
    }
    this.trigger("" + this.cid + ":" + (this.rendered ? 'update' : 'render'), this);
    return this;
  };

  /*
  Append current view to another view
  
  @param view - element/jQuery/View - an element, View or jQuery Object to add the current view to
  @return View - current view
  */


  View.prototype.appendTo = function(view) {
    var el;
    el = this.$el;
    if (view instanceof Backbone.View) {
      view.$el.append(el);
    } else {
      $(view).append(el);
    }
    this.render();
    this.trigger("" + this.cid + ":" + (this.rendered ? 'update' : 'render'), this);
    return this;
  };

  /*
  Prepend a view to the current view.
  
  @param view - element/jQuery/View - an element, View or jQuery Object to prepend to the current view's DOM element
  @return View - current view
  */


  View.prototype.prepend = function(view) {
    var el;
    el = this.$el;
    if (view instanceof Backbone.View) {
      view.$el.prepend(el);
    } else {
      $(view).prepend(el);
    }
    this.render();
    this.trigger("" + this.cid + ":" + (this.rendered ? 'update' : 'render'), this);
    return this;
  };

  /*
  Prepend current view to another view
  
  @param view - element/jQuery/View - an element, View or jQuery Object to prepend the current view to
  @return View - current view
  */


  View.prototype.prependTo = function(view) {
    var el;
    el = this.$el;
    if (view instanceof Backbone.View) {
      view.$el.prepend(el);
    } else {
      $(view).prepend(el);
    }
    this.render();
    this.trigger("" + this.cid + ":" + (this.rendered ? 'update' : 'render'), this);
    return this;
  };

  /*
  Insert current view after another view
  
  @param view - element/jQuery/View - an element, View or jQuery Object to insert the current view after
  @return View - current view
  */


  View.prototype.after = function(dom) {
    this.$el.after(dom);
    this.trigger("" + this.cid + ":update", this);
    return this.$el;
  };

  /*
  Insert current view before another view
  
  @param view - element/jQuery/View - an element, View or jQuery Object to insert the current view before
  @return View - current view
  */


  View.prototype.before = function(dom) {
    this.$el.after(dom);
    this.trigger("" + this.cid + ":update", this);
    return this.$el;
  };

  /*
  Insert current view before another view
  
  @param css - Add css changes to the dom element
  @return element
  */


  View.prototype.css = function(css) {
    this.$el.css(css);
    this.trigger("" + this.cid + ":update", this);
    return this.$el;
  };

  /*
  After render hook
  @return this
  */


  View.prototype.afterRender = function() {
    if (this.model != null) {
      this.listenTo(this.model, 'destroy', this.remove);
    }
    return this;
  };

  /*
  Before render hook
  @return this
  */


  View.prototype.beforeRender = function() {
    return this;
  };

  /*
  Initialize any models required for the view
  */


  View.prototype.prepareModels = function() {};

  /*
  Initialize any subviews required for the view
  */


  View.prototype.prepareViews = function() {};

  /*
  Initialize any routers required for the view
  */


  View.prototype.prepareRouters = function() {};

  return View;

})(Backbone.View);

module.exports = View;
});

;require.register("models/user_model", function(exports, require, module) {
var UserModel, _ref,
  __hasProp = {}.hasOwnProperty,
  __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

UserModel = (function(_super) {
  __extends(UserModel, _super);

  function UserModel() {
    _ref = UserModel.__super__.constructor.apply(this, arguments);
    return _ref;
  }

  return UserModel;

})(Model);

module.exports = UserModel;
});

;require.register("routers/main_router", function(exports, require, module) {
var MainRouter, _ref,
  __hasProp = {}.hasOwnProperty,
  __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

MainRouter = (function(_super) {
  __extends(MainRouter, _super);

  function MainRouter() {
    _ref = MainRouter.__super__.constructor.apply(this, arguments);
    return _ref;
  }

  MainRouter.prototype.routes = {
    "example": 'loadExample'
  };

  MainRouter.prototype.loadExample = function() {};

  return MainRouter;

})(Router);

module.exports = MainRouter;
});

;require.register("templates/components/main", function(exports, require, module) {
var __templateData = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  var buffer = "", stack1, functionType="function", escapeExpression=this.escapeExpression;


  if (stack1 = helpers.test) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.test; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "\n<h2>APPLICATION BOOTSTRAP</h2>";
  return buffer;
  });
if (typeof define === 'function' && define.amd) {
  define([], function() {
    return __templateData;
  });
} else if (typeof module === 'object' && module && module.exports) {
  module.exports = __templateData;
} else {
  __templateData;
}
});

;require.register("utils/class_utils", function(exports, require, module) {
var ClassUtils;

ClassUtils = (function() {
  function ClassUtils() {}

  ClassUtils.getClass = function(item) {
    return Object.prototype.toString.call(item);
  };

  return ClassUtils;

})();

module.exports = ClassUtils;
});

;require.register("utils/date_utils", function(exports, require, module) {
var DateUtils;

DateUtils = (function() {
  function DateUtils() {}

  DateUtils.YEAR_IN_SECONDS = 31536000;

  DateUtils.MONTHS_IN_SECONDS = 2592000;

  DateUtils.DAYS_IN_SECONDS = 86400;

  DateUtils.HOUR_IN_SECONDS = 3600;

  DateUtils.MINUTES_IN_SECONDS = 60;

  DateUtils.timeAgo = function(date) {
    var interval, seconds;
    date = new Date(date);
    seconds = Math.floor((new Date() - date) / 1000);
    interval = Math.floor(seconds / this.YEAR_IN_SECONDS);
    if (interval >= 1) {
      return "" + interval + " year" + (interval > 1 ? 's' : '') + " ago";
    }
    interval = Math.floor(seconds / this.MONTHS_IN_SECONDS);
    if (interval >= 1) {
      return "" + interval + " month" + (interval > 1 ? 's' : '') + " ago";
    }
    interval = Math.floor(seconds / this.DAYS_IN_SECONDS);
    if (interval >= 1) {
      return "" + interval + " day" + (interval > 1 ? 's' : '') + " ago";
    }
    interval = Math.floor(seconds / this.HOUR_IN_SECONDS);
    if (interval >= 1) {
      return "" + interval + " hour" + (interval > 1 ? 's' : '') + " ago";
    }
    interval = Math.floor(seconds / this.MINUTES_IN_SECONDS);
    if (interval >= 2) {
      return "" + interval + " minute" + (interval > 1 ? 's' : '') + " ago";
    }
    return "Just now";
  };

  return DateUtils;

})();

module.exports = DateUtils;
});

;require.register("utils/logger_util", function(exports, require, module) {
var LoggerUtil;

LoggerUtil = (function() {
  function LoggerUtil() {}

  LoggerUtil.debug = function() {
    var a, index, item, output, _i, _len;
    a = Array.prototype.slice.apply(arguments);
    output = "";
    index = 0;
    for (_i = 0, _len = a.length; _i < _len; _i++) {
      item = a[_i];
      index = index === 0 ? "->" : "";
      output = ("" + output + " " + index + " ") + item;
      index++;
    }
    return console.log("" + (new Date()), output.toString());
  };

  return LoggerUtil;

})();

module.exports = LoggerUtil;
});

;require.register("utils/path_utils", function(exports, require, module) {
var PathUtils;

PathUtils = (function() {
  function PathUtils() {}

  PathUtils.getQueryParamObject = function() {
    var hash, query, vars;
    query = window.location.search.substring(1);
    hash = {};
    if (_.isEmpty(query)) {
      return hash;
    }
    vars = query.split("&");
    _.each(vars, function(el, index) {
      var pair;
      pair = el.split("=");
      return hash[decodeURIComponent(pair[0])] = decodeURIComponent(pair[1]);
    });
    return hash;
  };

  PathUtils.removeUrlParam = function(key) {
    var i, pairs, prefix, search, url, urlparts;
    search = '';
    url = window.location.search.split('?');
    urlparts = url;
    if (urlparts.length >= 2) {
      prefix = encodeURIComponent(key) + "=";
      pairs = urlparts[1].split(/[&;]/g);
      i = pairs.length;
      while (i-- > 0) {
        if (pairs[i].lastIndexOf(prefix, 0) !== -1) {
          pairs.splice(i, 1);
        }
        search = urlparts[0] + "?" + pairs.join("&");
      }
    } else {
      search;
    }
    search = search === "?" ? "" : search;
    return window.history.pushState({}, document.title, "" + window.location.pathname + search);
  };

  PathUtils.getQueryPath = function(hash, extend) {
    var url;
    if (_.isEmpty(hash)) {
      return;
    }
    extend = extend || {};
    hash = _.extend(this.getQueryParamObject(), hash, extend);
    url = "";
    _.each(hash, function(value, key) {
      var end, pad;
      pad = url === "" ? "?" : "";
      end = url === "" ? "" : "&";
      return url = "" + url + pad + key + "=" + value + "&";
    });
    return url.slice(0, url.length - 1);
  };

  PathUtils.getUpdatedUrlPath = function(hash, extend) {
    var url;
    if (_.isEmpty(hash)) {
      return;
    }
    url = this.getQueryPath(hash, extend);
    return "" + window.location.pathname + url;
  };

  PathUtils.updateUrlParams = function(hash, extend, replaceState) {
    if (replaceState) {
      return window.history.replaceState({}, document.title, this.getUpdatedUrlPath(hash, extend));
    } else {
      return window.history.pushState({}, document.title, this.getUpdatedUrlPath(hash, extend));
    }
  };

  PathUtils.getParameterByName = function(name) {
    var regex, results;
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    regex = new RegExp("[\\?&]" + name + "=([^&#]*)");
    results = regex.exec(location.search);
    if (results == null) {
      return "";
    } else {
      return decodeURIComponent(results[1].replace(/\+/g, " "));
    }
  };

  return PathUtils;

})();

module.exports = PathUtils;
});

;require.register("utils/popup_utils", function(exports, require, module) {
var PopUpUtil;

PopUpUtil = (function() {
  function PopUpUtil() {}

  PopUpUtil.popup = function(url, windowWidth, windowHeight) {
    var documentElement, dualScreenLeft, dualScreenTop, height, left, open, top, width;
    documentElement = document.documentElement;
    dualScreenLeft = ((typeof window !== "undefined" && window !== null ? window.screenLeft : void 0) ? window.screenLeft : screen.left);
    dualScreenTop = ((typeof window !== "undefined" && window !== null ? window.screenTop : void 0) ? window.screenTop : screen.top);
    width = window.innerWidth || documentElement.clientWidth || screen.width;
    height = window.innerHeight || documentElement.clientHeight || screen.height;
    left = ((width - windowWidth) * 0.5) + dualScreenLeft;
    top = ((height - windowHeight) * 0.5) + dualScreenTop;
    return (open = function(url) {
      var popup;
      return popup = window.open(url, "_blank", "resizeable=true,height=" + windowHeight + ",width=" + windowWidth + ",left=" + left + ",top=" + top);
    })(url);
  };

  return PopUpUtil;

})();

module.exports = PopUpUtil;
});

;require.register("utils/resize_util", function(exports, require, module) {
var ResizeUtil;

ResizeUtil = (function() {
  function ResizeUtil() {}

  ResizeUtil.init = function() {
    this.updateLayout = _.debounce(function(e) {
      return Application.trigger('stageResize');
    }, Application.resizeDelay || 500);
    return window.addEventListener("resize", this.updateLayout, false);
  };

  return ResizeUtil;

})();

module.exports = ResizeUtil;
});

;require.register("utils/script_utils", function(exports, require, module) {
var ScriptUtils, o;

o = {
  '.py': 'python',
  '.js': 'javascript',
  '.coffee': 'coffeescript'
};

ScriptUtils = (function() {
  function ScriptUtils() {}

  ScriptUtils.getLangByFileType = function(name) {
    var k, match, v;
    match = false;
    for (k in o) {
      v = o[k];
      if (!(name.match(new RegExp(k)))) {
        continue;
      }
      if (v) {
        match = true;
      }
      break;
    }
    if (match) {
      return v;
    }
  };

  return ScriptUtils;

})();

module.exports = ScriptUtils;
});

;require.register("utils/tracking_util", function(exports, require, module) {
var TrackingUtil;

TrackingUtil = (function() {
  function TrackingUtil() {}

  TrackingUtil.trackClick = function(view, eventLabel, currentTargetClassName, href) {
    var o, userId, viewClassName;
    userId = Application.userModel.isLoggedIn() ? Application.userModel.get('id') : "??";
    currentTargetClassName = currentTargetClassName || '';
    viewClassName = (view != null ? view.className : void 0) || '';
    eventLabel = eventLabel || 'no_event_label';
    o = {
      userId: userId,
      viewClassName: view != null ? view.className : void 0,
      eventLabel: eventLabel,
      currentTarget: currentTargetClassName,
      href: href,
      date: new Date()
    };
    Logger.debug('TrackingUtil.trackClick');
    console.log('trackingClick', viewClassName, eventLabel, currentTargetClassName, JSON.stringify(o));
    return ga('send', 'event', 'click', view != null ? view.className : void 0, eventLabel, JSON.stringify(o));
  };

  return TrackingUtil;

})();

module.exports = TrackingUtil;
});

;require.register("views/main_view", function(exports, require, module) {
var MainView, template, _ref,
  __hasProp = {}.hasOwnProperty,
  __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

template = require('templates/components/main');

MainView = (function(_super) {
  __extends(MainView, _super);

  function MainView() {
    _ref = MainView.__super__.constructor.apply(this, arguments);
    return _ref;
  }

  MainView.prototype.autoRender = true;

  MainView.prototype.initialize = function(options) {
    this.model = new Model({
      test: 'Hello'
    });
    return MainView.__super__.initialize.call(this, options);
  };

  MainView.prototype.template = function() {
    return template;
  };

  return MainView;

})(View);

module.exports = MainView;
});

;
//# sourceMappingURL=app.js.map