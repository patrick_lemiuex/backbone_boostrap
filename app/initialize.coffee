@Application ?= { host: "http://#{window.location.host}"}
@View = require './lib/view'
@Router = require './lib/router'
@Model = require './lib/model'
@Collection = require './lib/collection'
@LocalStorageCollection = require './lib/local_storage_collection'
@MainView = require './views/main_view'
@PathUtils = require './utils/path_utils'
@Logger = require './utils/logger_util'
@DateUtils = require './utils/date_utils'
@ScriptUtils = require './utils/script_utils'
@TrackingUtil = require './utils/tracking_util'
@isTouchDevice =  (/android|webos|iphone|ipod|ipad|blackberry|iemobile/i.test(navigator.userAgent.toLowerCase()) )

$(document).ready ->
  _.extend(Application, Backbone.Events)
  mainView = new MainView()
  $('body').append( mainView.$el )
  return