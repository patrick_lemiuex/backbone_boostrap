module.exports =
Handlebars.registerHelper "pluralize", (num, singular, plural) ->
  if parseInt(num,10) is 1
    singular
  else
    (if typeof plural is "string" then plural else singular + "s")

Handlebars.registerHelper "pluralCount", (num, singular, plural) ->
  number + " " + Handlebars.helpers.pluralize.apply(this, arguments)

#  format an ISO date using Moment.js
#  http://momentjs.com/
#  moment syntax example: moment(Date("2011-07-18T15:50:52")).format("MMMM YYYY")
#  usage: {{dateFormat creation_date format="MMMM YYYY"}}
Handlebars.registerHelper "dateFormat", (context, block) ->
  if window.moment
    f = block.hash.format or "MMM DD, YYYY hh:mm:ss A"
    return moment(context).zone((new Date()).getTimezoneOffset()).format(f) #had to remove Date(context)
  else
    return context #  moment plugin not available. return data as is.