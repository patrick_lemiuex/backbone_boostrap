class PagingCollection extends Collection
  ignore: ['length','id','appId','_listenId','count']
  defaults: {page_size: 10, page: 1 }
  idAttribute: 'appId'
  url: ->
    params = _({}).extend(@defaults)
    for own k, v of this when _(v).isString() or _(v).isNumber()
      params[k] = v unless k in @ignore
    return "#?#{$.param(params)}"

  initialize: (models, options={}) ->
    @appId  = options?.appId
    _(this).extend(options)

  parse: (response) ->
    @page = response?.page or 1
    @count = response?.count
    return response?.results or []
  nextPage: -> @goToPage((@page or 0) + 1)
  previousPage: -> @goToPage((@page or 1) - 1)
  goToPage: (@page=0) -> @fetch()