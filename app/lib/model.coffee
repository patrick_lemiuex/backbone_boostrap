class Model extends Backbone.Model
  constructor: (attrs,options) ->
    super(attrs,options)

  initialize: (attrs,options)  ->
    @url = attrs?.url if attrs?.url
    @set(guid: @generateGuid() ) unless @get('guid')
    super(attrs,options)

  @generateGuid: ->
    S4 = -> (((1 + Math.random()) * 0x10000) | 0).toString(16).substring 1
    guid = -> S4() + S4() + "-" + S4() + "-" + S4() + "-" + S4() + "-" + S4() + S4() + S4()
    guid()

  generateGuid: ->
    S4 = -> (((1 + Math.random()) * 0x10000) | 0).toString(16).substring 1
    guid = -> S4() + S4() + "-" + S4() + "-" + S4() + "-" + S4() + "-" + S4() + S4() + S4()
    guid()

module.exports = Model