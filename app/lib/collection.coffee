class Collection extends Backbone.Collection
  constructor: (items,defaults) ->
    @searchFields = defaults?.searchFields or ['summary','name']
    super(items,defaults)
  search: (query, callback) ->
    searchFields = @searchFields
    pattern = new RegExp($.trim(query).replace(RegExp(" ", "g"), "|"), "i")
    context = @filteredAppCollection or this
    matches = context.filter((model) ->
      doesMatch = false
      for field in searchFields
        return false unless field
        doesMatch = true for k,v of model.attributes when k is field and v.match(pattern)
        return doesMatch
    )
    callback.call this, matches
    return

module.exports = Collection