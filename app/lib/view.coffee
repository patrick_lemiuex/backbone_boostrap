Model = require 'lib/model'
class View extends Backbone.View
  name: null
  autoRender: off
  autoRemove: on
  rendered: no
  template: -> ''
  ###
  @return instance of View
  ###
  constructor: (options) ->
    #Application.currentViews.push(this) unless @noRemove
    super(options)

  ###
  Kicks off the shit
  @return null
  ###
  initialize: ->
    #@listenTo(@model.collection, 'remove', @remove) if @model?.collection and @autoRemove
    Application.viewMap = [] unless Application?.viewMap
    @listenTo(@model, 'remove', @remove) if @model? and @autoRemove
    @name = @name or @constructor.name
    @render() if @autoRender is on
    @trigger "#{@cid}:initialize", this
    @prepareModels()
    @prepareViews()

  ###
  Mixin'd in views
  example: @include ('Coverflow')
  ###
  events: ->
    events = {}
    _(events).extend(v) for k, v of this when /[\w+]+_events$/.test(k)
    for k, v of events when /click/.test(k) and isTouchDevice
      mobileKey = k.replace('click','touchstart')
      events[ mobileKey ] = v
      delete events[ k ]
    return events

  ###
  Statics:
  Include a mixin, found in ./views/mixins
  example: @include ('Coverflow')
  ###
  @include: (mixins...) ->
    for mixin in mixins
      for own key, value of Application.mixins[mixin]::
        this::[key] = value

  ###
  Include a mixin, found in ./views/mixins
  example: include ('Coverflow')
  ###
  include: (mixins...) ->
    for mixin in mixins
      for own key, value of AgentBidder.mixins[mixin]::
        this[key] = value
    @["#{mixin}Init"]() if @["#{mixin}Init"]
    @delegateEvents()

  ###
  Dispatch an event to on the view's element

  @param eventName - String - name of event to dispatch
  @params ...rest - parameters to pass to event handlers
  @return View - current view
  ###
  dispatch: (eventName) ->
    arguments_ = arguments
    @$el.trigger eventName, _.rest(arguments_)
    this

  ###
  delegate events, called automaticall after render
  @param events  - object hash of events to listen for
  ###
  delegateEvents: (events) ->
    Backbone.View::delegateEvents.call this, events
    super( if _(@events?).isFunction() then @events() or @events )
    # bind the remove method to when the element is removed
    @$el.bind "DOMElementRemoved" + @eventNamespace, _.bind(@remove, this) if @eventNamespace

  ###
  convienence method for wiping out sub views
  ex: this.contentView.empty()

  ###
  empty: -> @$el.empty()

  ###
  Append current view to another view
  ex: this.contentView.appendToView(this.$('#content')) appends contentView to the dom element #content

  @param view - element/jQuery/View - an element, View or jQuery Object to add the current view to
  @return View - current view
  ###
  appendToView: (view) ->
    el = @$el
    if view instanceof Backbone.View
      view.$el.append el
    else
      $(view).append el
    @render()
    this

  ###
  Prepend current view to another view

  @param view - element/jQuery/View - an element, View or jQuery Object to prepend the current view to
  @return View - current view
  ###
  prependToView: (view) ->
    el = @$el
    if view instanceof Backbone.View
      view.$el.prepend el
    else
    $(view).prepend el
    @render()
    this

  ###
  Get the html buffer to render into the element

  @return Object - property object to render into html
  ###
  getRenderBuffer: (data) ->
    @template( data )

  ###
  Render the view with the provided buffer.

  @param buffer - String - html to render into the view.
  ###
  renderBuffer: (buffer) ->
    @$el.html( buffer )

  ###
  Get the model data to render the view.
  @return Object - property object that view will be rendered from
  ###
  getRenderData: ->
    o = { host: Application.host }
    _.extend({}, o, @model?.toJSON() )


  ###
  Get the model data to render the view.
  @return Object - property object that view will be rendered from
  ###
  render: ->
    @trigger "#{@cid}:render:before", this
    @beforeRender()
    @$el.attr('data-cid', @cid)
    if @model
      @$el.attr('data-model-cid', @model?.cid)
      @$el.attr('data-model-id', @model?.id)
    @html @template(@getRenderData())
    @rendered = yes
    Application.viewMap[@cid] = this
    @trigger "#{@cid}:render:after", this
    @afterRender()
    this

  ###
  Get the model data to render the view.
  @return Object - Convenience function for doToggle
  ###
  toggleClass: (className, doToggle) ->
    @$el.toggleClass(className, doToggle)
    this

  html: (dom) ->
    @$el.html(dom)
    @trigger "#{@cid}:#{if @rendered then 'update' else 'render'}", this
    @$el

  ###
  Append a view to the current view
  ex: this.append(this.contentView)

  @param view - element/jQuery/View - an element, View or jQuery Object to append to the current view's DOM element
  @return View - current view
  ###
  append: (view) ->
    if view instanceof View
      # call the appendTo method on the view
      view.appendTo this
    else if view instanceof Backbone.View
      @$el.append view.$el
      view.render()
    else
      # it's a DOM element, just append to our view
      @$el.append view
    @trigger "#{@cid}:#{if @rendered then 'update' else 'render'}", this
    this

  ###
  Insert a view into the currentView's element, replacing it

  @param view - element/jQuery/View - an element, View or jQuery Object
  @param el - element you wan to insert at in the current view
  @return View - current view
  ###
  insertAtElement: (view, el) ->
    #el = $(el) unless el instanceof jQuery #TODO Why doesn't this work? - login view doesn't like it
    if view instanceof View
      # call the appendTo method on the view
      view.render() unless view?.$el
      view.appendTo @$(el)
    else if view instanceof Backbone.View
      @$(el).append view.$el
      view.render()
    else
      # it's a DOM element, just append to our view
      @$el.find(el).append view
    @trigger "#{@cid}:#{if @rendered then 'update' else 'render'}", this
    this

  ###
  Append current view to another view

  @param view - element/jQuery/View - an element, View or jQuery Object to add the current view to
  @return View - current view
  ###
  appendTo: (view) ->
    el = @$el
    if view instanceof Backbone.View
      view.$el.append el
    else
      $(view).append el
    @render()
    @trigger "#{@cid}:#{if @rendered then 'update' else 'render'}", this
    this

  ###
  Prepend a view to the current view.

  @param view - element/jQuery/View - an element, View or jQuery Object to prepend to the current view's DOM element
  @return View - current view
  ###
  prepend: (view) ->
    el = @$el
    if view instanceof Backbone.View
      view.$el.prepend el
    else
      $(view).prepend el
    @render()
    @trigger "#{@cid}:#{if @rendered then 'update' else 'render'}", this
    this

  ###
  Prepend current view to another view

  @param view - element/jQuery/View - an element, View or jQuery Object to prepend the current view to
  @return View - current view
  ###
  prependTo: (view) ->
    el = @$el
    if view instanceof Backbone.View
      view.$el.prepend el
    else
      $(view).prepend el
    @render()
    @trigger "#{@cid}:#{if @rendered then 'update' else 'render'}", this
    this

  ###
  Insert current view after another view

  @param view - element/jQuery/View - an element, View or jQuery Object to insert the current view after
  @return View - current view
  ###
  after: (dom) ->
    @$el.after(dom)
    @trigger "#{@cid}:update", this
    @$el

  ###
  Insert current view before another view

  @param view - element/jQuery/View - an element, View or jQuery Object to insert the current view before
  @return View - current view
  ###
  before: (dom) ->
    @$el.after(dom)
    @trigger "#{@cid}:update", this
    @$el

  ###
  Insert current view before another view

  @param css - Add css changes to the dom element
  @return element
  ###
  css: (css) ->
    @$el.css(css)
    @trigger "#{@cid}:update", this
    @$el

  ###
  After render hook
  @return this
  ###
  afterRender: ->
    @listenTo(@model,'destroy', @remove ) if @model?
    this

  ###
  Before render hook
  @return this
  ###
  beforeRender: ->
    this

  ###
  Initialize any models required for the view
  ###
  prepareModels: -> #noop


  ###
  Initialize any subviews required for the view
  ###
  prepareViews: -> #noop

  ###
  Initialize any routers required for the view
  ###
  prepareRouters: -> #noop


module.exports = View