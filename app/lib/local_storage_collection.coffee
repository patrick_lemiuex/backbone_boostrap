class LocalStorageCollection extends Collection
  cacheTTL: 60
  constructor: (objects,defaults) ->
    @name = localStorageName = defaults?.localStorageName or @localStorageName
    ts = localStorage.getItem("#{localStorageName}-timestamp")
    if ts
      Logger.debug localStorageName,' Timestamp:', ts, 'cacheTTL',@cacheTTL,'Seconds Living:',(new Date() - new Date(ts))/1000, 'Replace?',(new Date() - new Date(ts))/1000>@cacheTTL
    else
      Logger.debug localStorageName,' Never loaded, querying API server'

    if ts and (new Date() - new Date(ts))/1000<@cacheTTL and navigator?.onLine
      @sync = Backbone.LocalStorage.sync
    else
      @sync = Backbone.sync
      @listenTo(this, 'sync', @updateTimestamp )

    @localStorageCollection = new Store(localStorageName)
    super(objects,defaults)

  storeMany: (models) ->
    @store(m) for m in models

  store: (model) ->
    @localStorageCollection.create(model)

  destroy: (model) ->
    @localStorageCollection.destroy(model)

  findAll: ->
    model = @model or Model #use the model on collection if there is one
    ( new model(obj) for obj in @localStorageCollection?.findAll() )

  indexOfLocalStorage: (model) ->
    a = @findAll()
    result = (item for m in a when model.get('guid') is m.get('guid'))
    a.indexOf(result)

  updateTimestamp: ->
    localStorage.setItem "#{@name}-timestamp", new Date().toISOString()

  save: ->
    @store(m) for m in @models

module.exports = LocalStorageCollection