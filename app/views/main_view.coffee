template = require 'templates/components/main'
class MainView extends View
  autoRender: on
  initialize: ( options) ->
    @model = new Model(name:'Welcome')
    super(options)
  template: template

module.exports = MainView