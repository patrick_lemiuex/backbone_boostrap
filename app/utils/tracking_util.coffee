class TrackingUtil
  @trackClick = (view, eventLabel, currentTargetClassName, href ) ->
    userId = if Application.userModel.isLoggedIn() then Application.userModel.get('id') else "??"
    currentTargetClassName = currentTargetClassName or ''
    viewClassName = view?.className or ''
    eventLabel = eventLabel or 'no_event_label'
    o =
      userId: userId
      viewClassName: view?.className
      eventLabel: eventLabel
      currentTarget: currentTargetClassName
      href: href
      date:  new Date()
    Logger.debug 'TrackingUtil.trackClick'


    console.log 'trackingClick',viewClassName, eventLabel, currentTargetClassName, JSON.stringify(o)
    ga('send', 'event', 'click', view?.className, eventLabel, JSON.stringify(o) )

module.exports = TrackingUtil