class ResizeUtil
  @init: ->
    #TODO needs a throttle event, combined with debounce
    @updateLayout = _.debounce((e) ->
      Application.trigger('stageResize')
    , Application.resizeDelay or 500) # Maximum run of once per 500 milliseconds
    window.addEventListener "resize", @updateLayout, false
module.exports = ResizeUtil