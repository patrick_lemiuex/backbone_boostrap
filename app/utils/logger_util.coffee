class LoggerUtil
  @debug = ->
    a = Array.prototype.slice.apply(arguments)
    output = ""
    index = 0
    for item in a
      index = if index is 0 then "->" else ""
      output = "#{output} #{index} "+item
      index++
    console.log "#{new Date()}",output.toString()

module.exports = LoggerUtil