class DateUtils
  @YEAR_IN_SECONDS = 31536000
  @MONTHS_IN_SECONDS = 2592000
  @DAYS_IN_SECONDS = 86400
  @HOUR_IN_SECONDS = 3600
  @MINUTES_IN_SECONDS = 60
  @timeAgo: (date) ->
    #TODO seconds greater then 60 fall through the cracks here
    date = new Date(date)
    seconds = Math.floor((new Date() - date) / 1000)
    interval = Math.floor(seconds / @YEAR_IN_SECONDS )
    return "#{interval} year#{if interval>1 then 's' else ''} ago"  if interval >= 1
    interval = Math.floor(seconds / @MONTHS_IN_SECONDS)
    return "#{interval} month#{if interval>1 then 's' else ''} ago"  if interval >= 1
    interval = Math.floor(seconds / @DAYS_IN_SECONDS )
    return "#{interval} day#{if interval>1 then 's' else ''} ago"  if interval >= 1
    interval = Math.floor(seconds / @HOUR_IN_SECONDS )
    return "#{interval} hour#{if interval>1 then 's' else ''} ago"  if interval >= 1
    interval = Math.floor(seconds / @MINUTES_IN_SECONDS )
    return "#{interval} minute#{if interval>1 then 's' else ''} ago"  if interval >= 2
    "Just now"
module.exports = DateUtils