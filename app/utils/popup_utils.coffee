class PopUpUtil
  @popup: (url,windowWidth, windowHeight) ->
    documentElement = document.documentElement
    # Multi Screen Popup Positioning (http://stackoverflow.com/a/16861050)
    #   Credit: http://www.xtf.dk/2011/08/center-new-popup-window-even-on.html
    # Fixes dual-screen position                         Most browsers      Firefox
    dualScreenLeft = (if window?.screenLeft then window.screenLeft else screen.left)
    dualScreenTop = (if window?.screenTop then window.screenTop else screen.top)
    width = window.innerWidth or documentElement.clientWidth or screen.width
    height = window.innerHeight or documentElement.clientHeight or screen.height
    left = ((width - windowWidth) *0.5) + dualScreenLeft
    top = ((height - windowHeight)*0.5) + dualScreenTop
    # Create a function for reopening the popup, and assigning events to the new popup object
    # This is a fix whereby triggering the
    do open = (url) ->
      popup = window.open(url, "_blank", "resizeable=true,height=" + windowHeight + ",width=" + windowWidth + ",left=" + left + ",top=" + top)

module.exports = PopUpUtil