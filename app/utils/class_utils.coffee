class ClassUtils
  @getClass: ( item ) ->
    Object.prototype.toString.call( item )

module.exports = ClassUtils