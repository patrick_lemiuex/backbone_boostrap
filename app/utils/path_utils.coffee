class PathUtils

  @getQueryParamObject: ->
    query = window.location.search.substring(1)
    hash = {}
    return hash if _.isEmpty( query )
    vars = query.split("&")
    _.each vars,(el,index) ->
      pair = el.split("=")
      hash[ decodeURIComponent(pair[0])] = decodeURIComponent(pair[1])
    hash

  @removeUrlParam: ( key ) ->
    search = ''
    url = window.location.search.split('?')
    urlparts = url
    if urlparts.length >= 2
      prefix = encodeURIComponent(key) + "="
      pairs = urlparts[1].split(/[&;]/g)
      i = pairs.length
      while i-- > 0 #reverse iteration as may be destructive
        #idiom for string.startsWith
        pairs.splice i, 1  if pairs[i].lastIndexOf(prefix, 0) isnt -1
        search = urlparts[0] + "?" + pairs.join("&")
    else
      search
    search = if search is "?" then "" else search
    window.history.pushState({},document.title,"#{window.location.pathname}#{search}")

  @getQueryPath: (hash,extend) ->
    return  if _.isEmpty(hash)
    extend = extend or {}
    hash = _.extend( @getQueryParamObject(), hash, extend)
    url = ""
    _.each(hash,(value,key) ->
      pad = if url is "" then "?" else ""
      end = if url is "" then "" else "&"
      url = "#{url}#{pad}#{key}=#{value}&"
      )
    url.slice(0,url.length-1)

  @getUpdatedUrlPath: (hash,extend) ->
    return  if _.isEmpty(hash)
    url = @getQueryPath( hash, extend )
    "#{window.location.pathname}#{url}"

  @updateUrlParams: ( hash, extend, replaceState ) ->
    if replaceState
      window.history.replaceState({},document.title,@getUpdatedUrlPath( hash,extend ))
    else
      window.history.pushState({},document.title,@getUpdatedUrlPath( hash,extend ))

  @getParameterByName: (name) ->
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]")
    regex = new RegExp("[\\?&]" + name + "=([^&#]*)")
    results = regex.exec(location.search)
    (if not results? then "" else decodeURIComponent(results[1].replace(/\+/g, " ")))

module.exports = PathUtils