o =
    '.py' : 'python'
    '.js' : 'javascript'
    '.coffee' : 'coffeescript'
class ScriptUtils
  @getLangByFileType: ( name ) ->
    match = false
    for k,v of o when name.match( new RegExp( k ) )
      match = true if v
      break
    v if match
module.exports = ScriptUtils